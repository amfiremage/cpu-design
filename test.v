module test;
    reg [7:0] v1 = 5;
    reg [7:0] v2 = 2;
    reg [2:0] opcode = 0;
    reg [2:0] opflags = 0;
    reg [2:0] opflags = 0;
    initial begin
        #10 v2 = 3;
        #20 opcode = 1;
        #60 v2 = 2;
        
        #1000 $stop;
    end
    
    reg clk = 0;
    always #5 clk = ~clk;
    wire [7:0] out_1;
    ALU a1(out_1, v1, v2, opcode, opflags, clk);
    
    initial begin
        $monitor("At time0 %t, out_1 = %b (%0d); v2 = %b (%0d);",
                     $time, out_1, out_1, v2, v2);
    end
endmodule // test
